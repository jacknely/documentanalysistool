import React from "react";
import { AppBar, Toolbar, Typography } from "@material-ui/core";
import ShareIcon from "@material-ui/icons/Share";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles(() => ({
  typoStyles: {
    fontFamily: '"Helvetica Neue"',
    fontSize: 25,
    fontWeight: 400,
    flex: 1,
  },
}));

const Header = () => {
  const classes = useStyles();

  return (
    <AppBar position="sticky">
      <Toolbar>
        <Typography className={classes.typoStyles}>
          Document Analysis Tool
        </Typography>
        <ShareIcon />
      </Toolbar>
    </AppBar>
  );
};

export default Header;
