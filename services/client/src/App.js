import React from "react";
import { Grid, Typography } from "@material-ui/core";
import MyForm from "./components/Form";
import Dropzone from "./components/Dropzone";
import Results from "./components/Results";
import Header from "./Header";
import "./App.css";

function App() {
  return (
    <Grid container direction="column">
      <Grid item>
        <Header />
      </Grid>
      <div style={{ padding: 20 }}>
        <Grid container>
          <Grid item xs={false} sm={2} />
          <Grid item xs={12} sm={8}>
            <Grid container direction="column" spacing={2}>
              <Grid item>
                <h3>1. Choose number of common words to find</h3>
                <MyForm />
              </Grid>
              <Grid item>
                <h3>2. Drag or Open Files (.txt only)</h3>
                <Dropzone />
              </Grid>
              <Grid item>
                <h3>3. View Results</h3>
                <Results />
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={false} sm={2} />
        </Grid>
      </div>
    </Grid>
  );
}

export default App;
