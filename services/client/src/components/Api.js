import axios from "axios";

export async function sendData(data, number) {
  if (data !== undefined && data.length > 0) {
    const fd = new FormData();
    for (const k of data) {
      fd.append("files", k, k.name);
    }
    try {
      console.log(data);
      let res = await axios.post(
        `${process.env.REACT_APP_USERS_SERVICE_URL}/api/?number=${number}`,
        fd
      );
      return res.data;
    } catch (error) {
      console.log(error);
    }
  } else {
    return null;
  }
}
