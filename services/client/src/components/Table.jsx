import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles({
  table: {
    minWidth: 400,
  },
});

export default function ResultsTable(props) {
  const { words, documents, sentences } = props;
  const classes = useStyles();

  const rows = words.map((k, i) => {
    return { words: k, documents: documents[i], sentences: sentences[i] };
  });
  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
            <TableCell>Word</TableCell>
            <TableCell align="right">Documents</TableCell>
            <TableCell align="right">Sentences</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.words}>
              <TableCell component="th" scope="row">
                {row.words}
              </TableCell>
              <TableCell align="right">
                {row.documents.map((c) => {
                  return `${c}, `;
                })}
              </TableCell>
              <TableCell align="right">
                {row.sentences.map((c) => {
                  return `${c}, `;
                })}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
