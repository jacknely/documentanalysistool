import React from "react";
import { render, cleanup } from "@testing-library/react";
import { GlobalContext } from "../../context/GlobalState";

import Table from "../Table";

afterEach(cleanup);

it("renders with props", () => {
  const { getByLabelText, getByText } = render(
    <Table
      words={["test", "test2"]}
      documents={[["test1"], ["test2"]]}
      sentences={[["test3"], ["test4"]]}
    />
  );

  const table = getByLabelText("a dense table");
});
