import React from "react";
import { render, cleanup } from "@testing-library/react";
import { GlobalContext } from "../../context/GlobalState";

import MyForm from "../Form";

afterEach(cleanup);

it("renders with placeholder", () => {
  const { getByLabelText, getByText, container } = render(<MyForm />);

  const wordInput = container.querySelector('input[name="words"]');
  expect(wordInput).toHaveProperty("type", "text");
  expect(wordInput).toHaveProperty("placeholder", "Enter a number");
  expect(wordInput).not.toEqual();
});

it("renders", () => {
  const { asFragment } = render(<MyForm />);
  expect(asFragment()).toMatchSnapshot();
});
