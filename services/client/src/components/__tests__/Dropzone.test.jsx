import React from "react";
import { render, cleanup } from "@testing-library/react";
import { GlobalContext } from "../../context/GlobalState";

import Dropzone from "../Dropzone";

afterEach(cleanup);

it("renders with default props", () => {
  const { getByTestId, getByText, container } = render(<Dropzone />);

  const input = getByTestId("dropzone").querySelector("input[type='file']");
  expect(input).toHaveProperty("type", "file");
  expect(input).toMatchSnapshot();
});
