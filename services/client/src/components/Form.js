import React, { useContext } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { GlobalContext } from "../context/GlobalState";
import * as Yup from "yup";

import "./Form.css";

const MyForm = () => {
  const { number, updateNumber } = useContext(GlobalContext);

  return (
    <Formik
      initialValues={{
        words: "",
      }}
      validationSchema={Yup.object().shape({
        words: Yup.number()
          .required("Required")
          .integer("Must be integer")
          .positive("Must be positive"),
      })}
    >
      {(props) => {
        const {
          values,
          touched,
          errors,
          isSubmitting,
          handleChange,
          handleBlur,
          handleSubmit,
        } = props;
        return (
          <form onChange={handleChange}>
            <div className="field">
              <input
                name="words"
                id="input-words"
                value={values.words}
                type="text"
                placeholder="Enter a number"
                onBlur={handleBlur}
                onChange={(e) => {
                  handleChange(e);
                  updateNumber(+e.target.value);
                }}
              />
              <div className="input-feedback">{errors.words}</div>
            </div>
          </form>
        );
      }}
    </Formik>
  );
};

export default MyForm;
