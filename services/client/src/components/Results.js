import React, { useContext } from "react";
import { GlobalContext } from "../context/GlobalState";
import ResultsTable from "./Table";

const Results = () => {
  const { words, documents, sentences } = useContext(GlobalContext);

  const validInput = words.length > 0 ? true : false;

  return (
    <>
      {validInput ? (
        <ResultsTable
          words={words}
          documents={documents}
          sentences={sentences}
        />
      ) : (
        ""
      )}
    </>
  );
};

export default Results;
