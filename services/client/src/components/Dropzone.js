import React, { useMemo, useContext } from "react";
import { useDropzone } from "react-dropzone";
import { GlobalContext } from "../context/GlobalState";

const baseStyle = {
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  padding: "20px",
  borderWidth: 2,
  borderRadius: 2,
  borderColor: "#eeeeee",
  borderStyle: "dashed",
  backgroundColor: "#fafafa",
  color: "#bdbdbd",
  outline: "none",
  transition: "border .24s ease-in-out",
};

const activeStyle = {
  borderColor: "#2196f3",
};

const acceptStyle = {
  borderColor: "#00e676",
};

const rejectStyle = {
  borderColor: "#ff1744",
};

const Dropzone = (props) => {
  const { addFiles, files, deleteFile } = useContext(GlobalContext);

  const {
    acceptedFiles,
    rejectedFiles,
    getRootProps,
    getInputProps,
    isDragActive,
    isDragAccept,
    isDragReject,
    open,
  } = useDropzone({
    accept: ".txt",
    onDrop: (acceptedFiles) => {
      acceptedFiles.map((file) => addFiles(file));
    },
  });

  const style = useMemo(
    () => ({
      ...baseStyle,
      ...(isDragActive ? activeStyle : {}),
      ...(isDragAccept ? acceptStyle : {}),
      ...(isDragReject ? rejectStyle : {}),
    }),
    [isDragActive, isDragReject]
  );

  const validInput = files.length > 0 ? true : false;

  const fileList = files.map((item) => (
    <p key={item.name}>
      {item.name} <button onClick={() => deleteFile(item.name)}>Delete</button>
    </p>
  ));

  return (
    <>
      <div {...getRootProps({ style })} data-testid="dropzone">
        <input {...getInputProps()} />
        {isDragAccept && <p>All files will be accepted</p>}
        {isDragReject && <p>Some files will be rejected</p>}
        {!isDragActive && <p>Drop some files here ...</p>}
        <button type="button">Open File Dialog</button>
      </div>
      {validInput ? fileList : ""}
    </>
  );
};

export default Dropzone;
