// Reducer is used to specify applicatoin state changes based on actions
export default (state, action) => {
  switch (action.type) {
    case "ADD_FILES":
      return {
        ...state,
        files: [...state.files, action.payload],
      };
    case "UPDATE_FILES":
      return {
        ...state,
        ...action.payload,
      };

    case "DELETE_FILE":
      return {
        ...state,
        words: [],
        sentence: [],
        documents: [],
        files: state.files.filter((file) => file.name !== action.payload),
      };
    case "UPDATE_NUMBER":
      return {
        ...state,
        number: action.payload,
      };
    default:
      return state;
  }
};
