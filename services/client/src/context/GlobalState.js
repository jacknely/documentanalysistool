import React, { createContext, useReducer, useEffect } from "react";
import AppReducer from "./AppReducer";
import { sendData } from ".././components/Api";

//Intial State
const intialState = {
  files: [],
  number: 0,
  words: [],
  documents: [],
  sentences: [],
};

//Create context
export const GlobalContext = createContext(intialState);

// Provider so that apps can access
export const GlobalProvider = ({ children }) => {
  const [state, dispatch] = useReducer(AppReducer, intialState);

  const addFiles = (files) => {
    dispatch({
      type: "ADD_FILES",
      payload: files,
    });
  };

  const updateNumber = (number) => {
    dispatch({
      type: "UPDATE_NUMBER",
      payload: number,
    });
  };

  const deleteFile = (filename) => {
    dispatch({
      type: "DELETE_FILE",
      payload: filename,
    });
  };

  const updateFiles = async () => {
    if (state.number > 0) {
      const res = await sendData(state.files, state.number);
      dispatch({
        type: "UPDATE_FILES",
        payload: JSON.parse(res),
      });
    }
  };

  useEffect(() => {
    updateFiles();
  }, [state.files, state.number]);

  return (
    <GlobalContext.Provider
      value={{
        files: state.files,
        number: state.number,
        words: state.words,
        documents: state.documents,
        sentences: state.sentences,
        addFiles,
        updateFiles,
        updateNumber,
        deleteFile,
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};
