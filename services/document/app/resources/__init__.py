from flask_restx import Api

from app.resources.ping import ping_namespace
from app.resources.apiEP import doc_namespace

api = Api(version="1.0", title="Users API", doc="/doc")

api.add_namespace(ping_namespace, path="/ping")
api.add_namespace(doc_namespace, path="/api")
