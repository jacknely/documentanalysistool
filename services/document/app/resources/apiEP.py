from flask_restx import Resource, Api, reqparse, Namespace
from pathlib import Path
import json
from werkzeug.datastructures import FileStorage
from app.analysis.file import File
from app.analysis.collection import Collection
from app.analysis.document import Document


doc_namespace = Namespace("doc")

parser = reqparse.RequestParser()
parser.add_argument("files", type=FileStorage, location="files", action="append")
parser.add_argument("number")


class DocumentAnalysis(Resource):
    @doc_namespace.response(200, "test!")
    def get(self):
        return {"status": "success", "message": "pong"}

    @doc_namespace.response(201, "success!")
    def post(self):
        args = parser.parse_args()
        files = args["files"]

        folder = File.save_files(files)
        collection = Collection()
        files = Path(__file__).parent.parent / "files" / folder

        for file_item in files.iterdir():
            file = File(file_item)
            filename, contents = file.get_data()
            document = Document(filename, contents)
            collection.add(document)

        word_quantity = int(args["number"])
        common_words = collection.get_common_words(word_quantity)

        document_filenames = []
        sentences = []
        for word in common_words:
            sentences.append(collection.get_sentences(word))
            document_filenames.append(collection.get_documents(word))
        File.delete_files(folder)

        response = {
            "words": common_words,
            "documents": document_filenames,
            "sentences": sentences,
        }

        return json.dumps(response), 201


doc_namespace.add_resource(DocumentAnalysis, "")
