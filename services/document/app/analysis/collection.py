"""
This module represents a collection of documents.
"""
import re
from collections import Counter

from app.analysis.document import Document


class Collection:
    """
    This class hold a list of Document objects to allow for
    batch operations.
    """

    def __init__(self):
        self.documents = []

    def add(self, document: Document) -> None:
        """
        adds a Document object to
        collection
        :param document: Document Obj
        """
        self.documents.append(document)

    def get_common_words(self, amount: int) -> list:
        """
        returns a list of common word from
        Documents held in Collection
        :param amount: # of word to return
        :return: list of length amount
        """
        word_count = {}
        for doc in self.documents:
            for key, value in doc.get_word_count().items():
                word_count[key] = word_count.get(key, 0) + value
        words = Counter(word_count).most_common(amount)
        return list(map((lambda x: x[0]), words))

    def get_sentences(self, word: str) -> list:
        """
        returns a list of sentences containing
        a given work from Documents held in Collection
        :param word: str
        :return: list of sentences with word
        """
        sentences = []
        for doc in self.documents:
            sentences.append(doc.get_sentences_with_word(word))
        return sentences

    def get_documents(self, word: str) -> list:
        """
        returns a list of document filenames containing
        a given work from Documents held in Collection
        :param word: str
        :return: list of document filenames with word
        """
        document_filenames = [
            doc.name
            for doc in self.documents
            if re.sub(r"[^\w\s]", "", word) in doc.no_punctuation.split()
        ]
        return document_filenames
