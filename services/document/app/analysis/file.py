"""
This module handles file functions and data import.
"""
import os
import time
from pathlib import Path


class File:
    """
    The class representation of file operations required to
    import text documents to a form that can consumed by this app
    """

    def __init__(self, file: Path) -> None:
        self.set_file(file)

    @property
    def get_file(self) -> Path:
        return self.__file

    def set_file(self, file: Path) -> None:
        if file.is_file():
            self.__file = file
        else:
            raise FileNotFoundError(
                "The file is not in correct format "
                "or there are no files in given directory"
            )

    def get_data(self) -> tuple:
        """
        returns a file name and its
        contents
        :return: tuple of filename and contents
        """
        file = self.get_file
        data = open(file, "rb").read().decode("UTF-8")
        return file.stem, data

    @classmethod
    def save_files(cls, files: list) -> str:
        """
        saves a list of files in the "files"
        folder in app
        :param files: list of FileStorage objects
        :return: directory name where files saved
        """
        folder = time.strftime("%Y%m%d-%H%M%S")
        folder_path = Path(__file__).parent.parent / "files" / folder
        folder_path.mkdir(parents=True, exist_ok=True)
        for file in files:
            file.save(str(folder_path / file.filename))
        return folder

    @classmethod
    def delete_files(cls, folder_name: str) -> None:
        """
        removes the given directory in "files"
        folder in app
        :param folder_name: folder to be deleted
        """
        folder = Path(__file__).parent.parent / "files" / folder_name
        for file in folder.iterdir():
            os.remove(file)
        os.rmdir(folder)
