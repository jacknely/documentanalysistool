"""
This module represents a individual Document.
"""
import re
from pathlib import Path

import nltk

nltk.download("punkt")
nltk.download("stopwords")
nltk.data.path.append(str(Path(__file__).parent / "nltk_data"))


class Document:
    """
    This class is for a Document which contains properties:
    name and document
    """

    def __init__(self, name: str, document: str):
        self.document = document
        self.name = name
        self.sentences = nltk.sent_tokenize(self.document)
        self.no_punctuation = re.sub(r"[^\w\s]", "", self.document).lower()

    def get_sentences_with_word(self, word: str) -> list:
        """
        returns a list of sentences containing
        a given word
        :param word: str
        :return: list of sentences
        """

        sentences = filter(
            lambda sentence: re.sub(r"[^\w\s]", "", word)
            in self.strip_sentence(sentence).split(),
            self.sentences,
        )
        return list(sentences)

    @staticmethod
    def strip_sentence(sentence: str) -> str:
        """
        returns a sentence with punctuation
        removed
        :param sentence: str
        :return: stripped sentence as a str
        """
        return re.sub(r"[^\w\s]", "", sentence).lower()

    def get_word_count(self) -> dict:
        """
        returns a dictionary with counts for
        each word in document
        :return: dict
        """
        exclude_words = self.get_exclude_words()
        word_count = {}
        for word in self.no_punctuation.lower().split():
            if word not in exclude_words:
                word_count[word] = word_count.get(word, 0) + 1
        return word_count

    @staticmethod
    def get_exclude_words() -> list:
        """
        Retrieves stop words from NLTK corpus
        with additional words
        :return: List of excluded words
        """
        exclude_words = nltk.corpus.stopwords.words("english")
        exclude_words.extend(["-", ""])
        return exclude_words
