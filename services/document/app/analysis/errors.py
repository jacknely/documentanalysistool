"""
This module holds additional errors called in application
"""


class VeryBadError(Exception):
    """An error to be raised when something very bad happens."""
