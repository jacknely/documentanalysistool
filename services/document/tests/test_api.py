import pytest
from pathlib import Path

from app import create_app


@pytest.fixture(scope="module")
def test_app():
    app = create_app()
    app.config.from_object("app.config.TestingConfig")
    with app.app_context():
        yield app


def test_get(test_app):
    client = test_app.test_client()
    response = client.get("/api/")

    assert response.status_code == 200


def test_post(test_app):
    client = test_app.test_client()
    file = Path(__file__).parent / "test_docs/test_doc.txt"
    files = [
        (open(file, "rb"), "file.txt"),
        (open(file, "rb"), "file2.txt"),
    ]
    response = client.post(
        "/api/?number=2", data={"files": files}, content_type="multipart/form-data",
    )

    assert response.status_code == 201
