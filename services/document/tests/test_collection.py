from pathlib import Path

from app.analysis.collection import Collection
from app.analysis.document import Document
from app.analysis.file import File


class TestDocument:
    def setup_method(self):
        file_item = Path(__file__).parent / "test_docs/test_doc.txt"
        file = File(file_item)
        filename, contents = file.get_data()
        document = Document(filename, contents)
        self.collection = Collection()
        self.collection.add(document)

    def test_get_common_words(self):
        test_words = self.collection.get_common_words(2)

        assert test_words == ["see", "future"]

    def test_get_sentences(self):
        test_sentence = self.collection.get_sentences("see")

        assert "and see as I see" in list(test_sentence[0])[0]

    def test_get_documents(self):
        test_document = self.collection.get_documents("see")

        assert len(test_document) == 1
