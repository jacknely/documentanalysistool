from pathlib import Path

from app.analysis.document import Document
from app.analysis.file import File


class TestDocument:
    def setup_method(self):
        file_item = Path(__file__).parent / "test_docs/test_doc.txt"
        file = File(file_item)
        filename, contents = file.get_data()
        self.document = Document(filename, contents)

    def test_sentences(self):
        assert len(self.document.sentences) == 2

    def test_get_sentence_with_word(self):
        test_sentences = self.document.get_sentences_with_word("quest")
        assert len(list(test_sentences)) == 1

    def test_no_punctuation(self):
        test_no_punc = self.document.no_punctuation

        assert len(test_no_punc) == 492

    def test_strip_sentence(self):
        test_sentence = "this is, a, big, day."
        stripped = self.document.strip_sentence(test_sentence)

        assert stripped == "this is a big day"

    def test_get_word_count(self):
        count_test = self.document.get_word_count()

        assert len(count_test) == 42

    def test_get_exclude_words(self):
        assert len(self.document.get_exclude_words()) != 1
