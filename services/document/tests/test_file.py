from pathlib import Path

from werkzeug.datastructures import FileStorage

from app.analysis.file import File


class TestFile:
    def test_save_delete_files(self):
        files = [FileStorage(filename="test_doc.txt")]
        folder = File.save_files(files)
        expected_files = Path(__file__).parent.parent / "app/files" / folder

        assert expected_files.exists() is True

        File.delete_files(folder)

        assert expected_files.exists() is False

    def test_get_data(self):
        file_item = Path(__file__).parent / "test_docs/test_doc.txt"
        file = File(file_item)
        filename, contents = file.get_data()

        assert filename == "test_doc"
