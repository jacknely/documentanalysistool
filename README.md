[![pipeline status](https://gitlab.com/jacknely/documentanalysistool/badges/master/pipeline.svg)](https://gitlab.com/jacknely/documentanalysistool/commits/master)

# :green_book: Document Analysis Tool
Dynamic Full Stack Application that retrieves a list of common words from a directory of documents

![Document Analysis Tool Demo](demo/demo.gif)

## Hosted App
Production version of application is hosted on Heroku: https://arcane-harbor-07735.herokuapp.com/
